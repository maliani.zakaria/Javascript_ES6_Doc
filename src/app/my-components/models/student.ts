
/** 
 * Student Model
*/
export interface Student {

    firstname: string,
    lastname: string,
    notes: any
    
}
