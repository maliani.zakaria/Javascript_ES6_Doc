import { Component, OnInit } from '@angular/core';
import { StudentService } from './services/student.service';
import { Student } from './models/student';

@Component({
  selector: 'app-my-components',
  providers: [StudentService],
  templateUrl: './my-components.component.html',
  styleUrls: ['./my-components.component.css']
})
export class MyComponentsComponent implements OnInit {

  constructor(private studentService: StudentService) { }
 
  ngOnInit() {
    this.studentService.getStudents().catch(function(error) {
      console.log("controller error", error);
    }).then(
      function(response){
      }
    );
    };
    
  }

