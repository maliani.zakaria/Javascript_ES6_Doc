
import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Student } from '../models/student';
import { switchMap } from 'rxjs/operators/switchMap';
import { Observable } from "rxjs/Observable";

@Injectable()
export class StudentService {

  private wsUrl:string = "http://zakdev.com/students.json";

  constructor(private http: HttpClient) { }

  /**
   * Call WS GET STUDENT
   */
  getStudents() {

    let promise = new Promise((resolve, reject) => {
        this.http.get<Student[]>(this.wsUrl).toPromise().then(
            function(response){
              console.log("response" , response);
              resolve();
              
            },
            function(error){
              reject(error.message);
            }
        );
    })

    return promise;
  }

}
