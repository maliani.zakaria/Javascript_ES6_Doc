import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app IFIAG Licence pro';

  obj = {
    name : "ON est cool",
    society : "IFIAG"
  };

  arr = [{
    name : "ADIL FOUAD",
    society : "SOFRECOM"
  }, {
    name : "TARIK FULL",
    society : "MEDITEL"
  }, {
    name : "YOUNESS",
    society : "IFIAG"
  }];

  isTrue = false; 
  
  
}
